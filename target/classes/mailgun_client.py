import requests
import sys
import json
import urllib
import time
import datetime
from jinja2 import Template

build_number = sys.argv[1]
build_timestamp = int(time.time())
execution_date = datetime.datetime.utcfromtimestamp(build_timestamp).strftime('%m-%d-%Y %H:%M:%S')

json_url = "http://localhost:8080/job/CarePro-Test/%s/testReport/api/json" % build_number

try:
    json = requests.get(json_url, auth=("admin", "Healthy15")).json()
except:
    raise Exception("Build number: %s does not exist" % build_number)

executed = json['totalCount']
failed = json['failCount']
passed = json['totalCount'] - json['failCount'] - json['skipCount']
skipped = json['skipCount']
environment = 'RC'

with open('/Users/Shared/Jenkins/Home/workspace/CarePro-Test/src/main/resources/email_template.html', 'r') as html_template:
    html_content = html_template.read()

context = {
    "build_number": build_number,
    "execution_date": execution_date,
    "executed": executed,
    "failed": failed,
    "passed": passed,
    "skipped": skipped,
    "environment": environment
}

template = Template(html_content)
email_message = template.render(context)

response = requests.post(
    "https://api.mailgun.net/v3/sandbox33ee91af8431446d93f4be6c6ee85ebc.mailgun.org/messages",
    auth=("api", "key-01192b85ce81fa9e9d91a442d83c4469"),
    data={"from": "QA <mailgun@sandbox33ee91af8431446d93f4be6c6ee85ebc.mailgun.org>",
          "to": ["thediary@base2.com.br", "ruitao.s@thediary.com", "russ.t@thediary.com"],
          "subject": "[iOS, %s (%s of %s) passed, %s] - Automated Test Report %s" % (str(int(((float(passed) / float(executed)) * 100))) + "%", passed, str(int(executed)), environment, execution_date),
          "html": email_message}
)
