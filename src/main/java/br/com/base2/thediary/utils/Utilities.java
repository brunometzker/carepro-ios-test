package br.com.base2.thediary.utils;

import java.util.Random;
import java.util.UUID;

/**
 * Created by base2 on 5/23/17.
 */
public class Utilities {
    public static String generateRandomAlphanumericString() {
        return UUID.randomUUID().toString().substring(0, 8);
    }

    public static String generateRandomNumber() {
        return String.valueOf(new Random().nextInt(100000));
    }

    public static String generateRandomNumber(int to) {
        return String.valueOf(new Random().nextInt(to));
    }

    public static void sleep(int milliseconds){
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
