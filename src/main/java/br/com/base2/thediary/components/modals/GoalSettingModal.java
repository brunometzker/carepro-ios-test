package br.com.base2.thediary.components.modals;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by leonardoamaral on 09/03/17.
 */
public class GoalSettingModal extends FluentPage {
    @Step("{method}")
    public void tapDeleteButton() {
        el(By.name("DELETE")).click();
    }
}
