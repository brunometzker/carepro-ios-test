package br.com.base2.thediary.components.blocks;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by leonardoamaral on 13/03/17.
 */
public class EnrollmentTaskBlock extends FluentPage {
    @Step("{method}: {0}")
    public void tapEnrollmentTask(String enrollmentTask) {
        el(By.name(enrollmentTask)).axes().followingSiblings().get(1).click();
    }
}