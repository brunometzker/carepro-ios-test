package br.com.base2.thediary.components.modals;

import static org.assertj.core.api.Assertions.*;

import br.com.base2.thediary.config.Constants;
import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.concurrent.TimeUnit;

/**
 * Created by leonardoamaral on 13/03/17.
 */
public class EnrollmentTaskModal extends FluentPage {
    @Step("{method}")
    public void tapDoneButton() {
        el(By.name("Done")).click();
    }

    @Step("{method}")
    public void tapStatusLabel() {
        el(By.name("STATUS")).axes().followingSiblings().get(0).click();
    }

    @Step("{method}: {0}")
    public void verifyThatStatusIs(String status) {
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.name(status))).displayed());
    }
}
