package br.com.base2.thediary.components.modals;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by leonardoamaral on 08/03/17.
 */
public class CreateCarePlanModal extends FluentPage {
    @Step("{method}: {0}")
    public void fillCarePlanFocus(String careplanFocus) {
        el(By.name("Focus")).fill().with(careplanFocus);
    }

    @Step("{method}")
    public void tapSaveButton() {
        el(By.name("Save")).click();
    }
}
