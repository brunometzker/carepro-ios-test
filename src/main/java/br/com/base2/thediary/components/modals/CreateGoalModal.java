package br.com.base2.thediary.components.modals;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by leonardoamaral on 08/03/17.
 */
public class CreateGoalModal extends FluentPage {
    @Step("{method}: {0}")
    public void fillGoalField(String goal) {
        el(By.name("Goal")).fill().with(goal);
    }

    @Step("{method}: {0}")
    public void fillProblemField(String problem) {
        el(By.name("Problem")).fill().with(problem);
    }

    @Step("{method}")
    public void tapSaveButton() {
        el(By.name("Save")).click();
    }
}
