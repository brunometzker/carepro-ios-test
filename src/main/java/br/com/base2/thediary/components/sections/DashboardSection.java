package br.com.base2.thediary.components.sections;

import br.com.base2.thediary.components.blocks.NotesBlock;
import br.com.base2.thediary.components.blocks.ThresholdBlock;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;

/**
 * Created by leonardoamaral on 21/03/17.
 */
public class DashboardSection extends FluentPage {
    @Page private ThresholdBlock thresholdBlock;
    @Page private NotesBlock notesBlock;

    public ThresholdBlock atThresholdBlock() { return this.thresholdBlock; }

    public NotesBlock atNotesBlock() { return this.notesBlock; }
}
