package br.com.base2.thediary.components.modals;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by leonardoamaral on 09/03/17.
 */
public class TaskModal extends FluentPage {
    @Step("{method}: {0}")
    public void selectTaskType(String taskType) {
        el(By.name("Task Type")).click();
        el(By.name(taskType)).click();
    }

    @Step("{method}: {0}")
    public void fillTaskTitle(String taskTitle) {
        el(By.name("Task Title")).fill().with(taskTitle);
    }

    @Step("{method}: {0}")
    public void fillTaskDescription(String taskDescription) {
        el(By.name("Description")).fill().with(taskDescription);
    }

    @Step("{method}")
    public void tapSaveButton() {
        el(By.name("Save")).click();
    }
}
