package br.com.base2.thediary.components.tabbedbar;

import io.appium.java_client.AppiumDriver;
import javafx.scene.control.Tab;
import org.fluentlenium.core.FluentControl;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.components.ComponentInstantiator;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

/**
 * Created by leonardoamaral on 03/03/17.
 */
public class CareProTabs extends FluentPage {
    public void goToDashboard() {
        getDriver().findElement(By.name("Dashboard")).click();
    }

    public void goToOwners() {
        el(By.name("Owners")).click();
    }

    public void goToReports() {
        getDriver().findElement(By.name("Reports")).click();
    }

    public void goToAssessments() {
        getDriver().findElement(By.name("Assessments")).click();
    }

    public void goToNotifications() {
        getDriver().findElement(By.name("Notifications")).click();
    }

    public void goToResources() {
        getDriver().findElement(By.name("Resources")).click();
    }

    public void goToAccount() {
        getDriver().findElement(By.name("Account")).click();
    }
}