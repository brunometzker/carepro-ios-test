package br.com.base2.thediary.components.sections;

import br.com.base2.thediary.components.blocks.ModuleBlock;
import br.com.base2.thediary.components.blocks.ThresholdBlock;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;

/**
 * Created by leonardoamaral on 15/03/17.
 */
public class DiarySection extends FluentPage {
    @Page private ThresholdBlock thresholdBlock;
    @Page private ModuleBlock moduleBlock;

    public ThresholdBlock atThresholdBlock() { return this.thresholdBlock; }

    public ModuleBlock atModuleBlock(String moduleName) {
        this.moduleBlock.setModuleName(moduleName);
        return this.moduleBlock;
    }
}
