package br.com.base2.thediary.components.modals;

import org.fluentlenium.core.FluentPage;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by leonardoamaral on 09/03/17.
 */
public class CareteamTaskModal extends FluentPage {
    @Step("{method}")
    public void tapStatusLabel() {}

    @Step("{method}")
    public void tapDoneButton() {}

    @Step("{method}")
    public void tapDeleteButton() {}
}
