package br.com.base2.thediary.components.modals;

import io.appium.java_client.AppiumDriver;
import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by base2 on 5/25/17.
 */
public class NoteModal extends FluentPage {
    @Step("{method}: {0}")
    public void fillCareManagementTime(String time) {
        el(By.name("Care Management Time")).fill().with(time);
        ((AppiumDriver) getDriver()).hideKeyboard();
    }

    @Step("{method}: {0}")
    public void fillNote(String note) { el(By.name("Note")).fill().with(note); }

    @Step("{method}")
    public void tapSaveButton() { el(By.name("Save")).click(); }

    @Step("{method}")
    public void fillSubject(String subject) {
        el(By.name("Subject")).fill().with(subject);
        ((AppiumDriver) getDriver()).hideKeyboard();
    }

    @Step("{method}: {0}")
    public void verifyCareManagementTime(String careManagementTime){
        assertThat(el(By.name("Note")).attribute("value").equalsIgnoreCase(careManagementTime));
    }

    @Step("{method}: {0}")
    public void verifySubject(String subject){
        assertThat(el(By.name("Subject")).attribute("value").equalsIgnoreCase(subject));
    }

    @Step("{method}: {0}")
    public void verifyNote(String note){
        assertThat(el(By.name("Note")).attribute("value").equalsIgnoreCase(note));
    }


}
