package br.com.base2.thediary.components.blocks;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by leonardoamaral on 09/03/17.
 */
public class GoalBlock extends FluentPage {
    @Step("{method}: {0}")
    public void tapShowMoreButton(String goal) {
        el(By.name("Show more button for goal: " + goal)).click();
    }

    @Step("{method}: {0}")
    public void tapSettingButton(String goal) {
        el(By.name("Setting button for goal: " + goal)).click();
    }

    @Step("{method}")
    public void tapAddCareteamTaskButton() {
        el(By.xpath("//XCUIElementTypeStaticText[@name='CareTeam tasks']")).axes().followingSiblings().get(0).click();
    }

    @Step("{method}: {0}")
    public void tapCareteamTask(String careteamTask) {}

    @Step("{method}")
    public void verifyThatCareteamTaskIsDisplayed() {}

    @Step("{method}")
    public void verifyThatCareteamTaskIsActive() {}

    @Step("{method}")
    public void verifyThatCareteamTaskIsCompleted() {}

    @Step("{method}")
    public void verifyThatCareteamTaskIsNoLongerDisplayed() {}
}
