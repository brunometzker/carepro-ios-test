package br.com.base2.thediary.components.modals;

import br.com.base2.thediary.config.Constants;
import br.com.base2.thediary.utils.Utilities;
import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;
import static org.assertj.core.api.Assertions.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by leonardoamaral on 08/03/17.
 */
public class CreateOwnerModal extends FluentPage {
    @Step("{method}")
    public void tapCancelButton() {
        el(By.name("Cancel")).click();
    }

    @Step("{method}")
    public void tapSaveButton() {
        el(By.name("Save")).click();
        Utilities.sleep(4000);
    }

    @Step("{method}: {0}")
    public void fillFirstNameField(String firstName) {
        el(By.name("First Name")).fill().with(firstName);
    }

    @Step("{method}: {0}")
    public void fillSurnameField(String surname) {
        el(By.name("Last Name")).fill().with(surname);
    }

    @Step("{method}: {0}")
    public void selectGender(String gender) {
        el(By.name("Gender")).click();
        el(By.name(gender)).click();
    }

    @Step("{method}: {0}")
    public void selectPatientGroup(String patientGroup) {
        el(By.name("Groups")).click();
        await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.name(patientGroup))).displayed();
        el(By.name(patientGroup)).click();
        tapCreateOwnerButton();
    }

    @Step("{method}")
    public void tapCreateOwnerButton() {
        el(By.name("Create Owner")).click();
    }

    @Step("{method}: {0}")
    public void selectCareManager(String careManager) {
        el(By.name("Care Manager")).click();
        await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.name(careManager))).displayed();
        el(By.name(careManager)).click();
    }

    @Step("{method}")
    public void verifyThatRequiredFieldsMessageWasDisplayed() {
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS)
                   .until(el(By.name("Required"))));

        el(By.name("OKAY")).click();
    }
}
