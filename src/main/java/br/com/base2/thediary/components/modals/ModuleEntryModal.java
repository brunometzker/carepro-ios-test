package br.com.base2.thediary.components.modals;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by base2 on 5/19/17.
 */
public class ModuleEntryModal extends FluentPage {
    @Step("{method}: {0}")
    public void fillEntryValue(String value) {
        el(By.xpath("//XCUIElementTypeStaticText[@name='Value']//following-sibling::XCUIElementTypeTextField")).fill().with(value);
    }

    @Step("{method}")
    public void tapSaveButton() { el(By.name("Save")).click(); }
}