package br.com.base2.thediary.components.sections;

import static org.assertj.core.api.Assertions.*;

import br.com.base2.thediary.components.blocks.EnrollmentTaskBlock;
import br.com.base2.thediary.components.blocks.GoalBlock;
import br.com.base2.thediary.config.Constants;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.concurrent.TimeUnit;

/**
 * Created by leonardoamaral on 08/03/17.
 */
public class CarePlanSection extends FluentPage {
    @Page private EnrollmentTaskBlock enrollmentTaskBlock;
    @Page private GoalBlock goalBlock;

    @Step("{method}")
    public void tapCreateCarePlanButton() {
        el(By.name("Create Care Plan")).click();
    }

    @Step("{method}: {0}")
    public void verifyThatCarePlanIsDisplayed(String careplanFocus) {
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.name(careplanFocus))).displayed());
    }

    @Step("{method}: {0}")
    public void verifyThatGoalIsNoLongerDisplayed(String goal) {
        assertThat(not(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.name("Goal: " + goal))).displayed()));
    }

    @Step("{method}")
    public void tapAddProblemButton() {
        el(By.name("Add Problem")).click();
    }

    public GoalBlock atGoalBlock() {
        return this.goalBlock;
    }

    public EnrollmentTaskBlock atEnrollmentTaskBlock() { return this.enrollmentTaskBlock; }
}