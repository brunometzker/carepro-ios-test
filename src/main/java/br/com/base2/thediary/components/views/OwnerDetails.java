package br.com.base2.thediary.components.views;

import static org.assertj.core.api.Assertions.*;
import br.com.base2.thediary.components.sections.CarePlanSection;
import br.com.base2.thediary.components.sections.DashboardSection;
import br.com.base2.thediary.components.sections.DiarySection;
import br.com.base2.thediary.config.Constants;
import io.appium.java_client.AppiumDriver;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;
import java.util.concurrent.TimeUnit;

/**
 * Created by leonardoamaral on 07/03/17.
 */
public class OwnerDetails extends FluentPage {
    @Page private DashboardSection dashboardSection;
    @Page private CarePlanSection carePlanSection;
    @Page private DiarySection diarySection;

    public DashboardSection atDashboardSection() { return this.dashboardSection; }

    public CarePlanSection atCarePlanSection() {
        return this.carePlanSection;
    }

    public DiarySection atDiarySection() { return this.diarySection; }

    @Step("{method}")
    public void verifyThatOwnerIsNew() {
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.name("New"))).displayed());
    }

    @Step("{method}")
    public void verifyThatOwnerIsActive() {
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.name("Active"))).displayed());
    }

    @Step("{method}")
    public void tapOnCareplanSection() {
        el(By.name("Care Plan")).click();
    }

    @Step("{method}")
    public void tapOnDiarySection() { el(By.name("Diary")).click(); }

    @Step("{method}")
    public void tapAddNoteButton() {
        el(By.name("note")).click();
    }

    @Step("{method}")
    public void verifyThatCreateCareplanButtonIsDisplayed() {
        AppiumDriver appiumDriver = (AppiumDriver) getDriver();
        System.out.println(appiumDriver.getPageSource());
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.name("Create Care Plan"))).displayed());
    }

    @Step("{method}")
    public void verifyThatExistingCareplanIsDisplayed() {
        System.out.println(getDriver().getPageSource());
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.name("total_tasks"))).displayed());
    }

    @Step("{method}")
    public void verifyThatExistingGoalIsDisplayed() {
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.name("Goal: Quit Smoking"))).displayed());
    }

    @Step("{method}: {0}")
    public void verifyThatCreatedGoalIsDisplayed(String goal) {
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.name("Goal: " + goal))).displayed());
    }

    @Step("{method}: {0}")
    public void verifyThatBannerInformationIsDisplayed(String ownerName) {
        verifyThatOwnerNameIsDisplayed(ownerName);
        verifyThatOwnerPhotoIsDisplayed(ownerName);
    }

    @Step("{method}: {0}")
    public void verifyThatOwnerPhotoIsDisplayed(String ownerName) {
        String initials = ownerName.split(" ")[0] + ownerName.split(" ")[1];

        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(
             el(By.xpath("//XCUIElementTypeStaticText[@name='${initials}']//following-sibling::XCUIElementTypeImage"
                         .replace("${initials}", initials)))
        ));
    }

    @Step("{method}: {0}")
    public void verifyThatOwnerNameIsDisplayed(String ownerName) {
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(
            el(By.name(ownerName))
        ));
    }
}
