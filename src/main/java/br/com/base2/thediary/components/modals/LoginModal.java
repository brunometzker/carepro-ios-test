package br.com.base2.thediary.components.modals;


import static org.assertj.core.api.Assertions.*;

import br.com.base2.thediary.config.Constants;
import io.appium.java_client.AppiumDriver;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.concurrent.TimeUnit;

/**
 * Created by bmetzker on 24/02/17.
 */
public class LoginModal extends FluentPage {
    @Step("{method}: {0}")
    public void fillUsernameWith(String username) {
        el(By.id("login_email")).fill().with(username);
    }

    @Step("{method}: {0}")
    public void fillPasswordWith(String password) {
        el(By.id("login_password")).fill().with(password);
    }

    @Step("{method}")
    public void submitCredentials() {
        el(By.id("login_submit")).click();
    }

    @Step("{method}")
    public void closeModal() { el(By.name("iconoverlayClose")).click(); }

    @Step("{method}: {0}")
    public void verifyThatErrorMessageIs(String errorMessage) {
        String alert = "SVProgressHUD";
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS)
                .until(el(By.name(alert))).displayed());
    }
}
