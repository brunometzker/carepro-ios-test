package br.com.base2.thediary.components.modals;

import br.com.base2.thediary.config.Constants;
import io.appium.java_client.AppiumDriver;
import org.assertj.core.api.Assertions;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by leonardoamaral on 15/03/17.
 */
public class NewThresholdModal extends FluentPage {
    @Step("{method}: {0}")
    public void selectThresholdModule(String thresholdName) {
        el(By.name("Module")).click();
        el(By.name(thresholdName)).click();
    }

    @Step("{method}: {0}")
    public void selectSeverityLevel(String severityLevel) {
        el(By.name("Severity")).click();
        el(By.className("XCUIElementTypePickerWheel"));
        el(By.name(severityLevel)).click();
    }

    @Step("{method}: {0}")
    public void selectNotificationMethod(String notificationMethod) {
        el(By.name("Notification ")).click();
        el(By.name(notificationMethod)).click();
        el(By.name("Threshold")).click();
    }

    @Step("{method}: {0}")
    public void fillInMinimunLimit(String minimunLimit) {
        el(By.name("Min")).fill().with(minimunLimit);
    }

    @Step("{method}: {0}")
    public void fillInMaximumLimit(String maximumLimit) {
        el(By.name("Max")).fill().with(maximumLimit);
    }

    @Step("{method}: {0}")
    public void fillInSystolicMinimum(String min) {
        $(By.name("Systolic")).index(0).fill().with(min);
        ((AppiumDriver) getDriver()).hideKeyboard();
    }

    @Step("{method}: {0}")
    public void fillInSystolicMaximum(String max) {
        $(By.name("Systolic")).index(1).fill().with(max);
        ((AppiumDriver) getDriver()).hideKeyboard();
    }

    @Step("{method}: {0}")
    public void fillInDiastolicMinimun(String min) {
        $(By.name("Diastolic")).index(0).fill().with(min);
    }

    @Step("{method}: {0}")
    public void fillInDiastolicMaximum(String max) {
        $(By.name("Diastolic")).index(1).fill().with(max);
    }

    @Step("{method}")
    public void tapSaveButton() { el(By.name("Save")).click(); }

    @Step("{method}")
    public void tapCancelButton() { el(By.name("Cancel")).click(); }

    @Step("{method}")
    public void verifyThatModalIsDisplayed() {
        FluentWebElement modalTitle = el(By.name("Threshold"));

        await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()))
               .until(modalTitle)
               .displayed();

        assertThat(el(By.name("Threshold")).displayed());
    }
}