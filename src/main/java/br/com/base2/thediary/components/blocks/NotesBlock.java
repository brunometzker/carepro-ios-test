package br.com.base2.thediary.components.blocks;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by base2 on 5/25/17.
 */
public class NotesBlock extends FluentPage {
    @Step("{method}: {0}")
    public void verifyThatNoteIsDisplayed(String note) {
        assertThat(el(By.name(note)).displayed());
    }

    @Step("{method}: {0}")
    public void tapOpenNote(String note){
        el(By.name(note)).click();
    }
}
