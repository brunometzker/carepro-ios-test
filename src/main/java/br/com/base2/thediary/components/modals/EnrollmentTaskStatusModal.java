package br.com.base2.thediary.components.modals;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by leonardoamaral on 13/03/17.
 */
public class EnrollmentTaskStatusModal extends FluentPage {
    @Step("{method}: {0}")
    public void updateStatus(String status) {
        el(By.name("Status")).click();
        el(By.name(status)).click();
    }

    @Step("{method}")
    public void tapSaveButton() {
        el(By.name("Save")).click();
    }
}
