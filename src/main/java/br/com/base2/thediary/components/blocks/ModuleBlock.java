package br.com.base2.thediary.components.blocks;

import br.com.base2.thediary.appium.Actions;
import br.com.base2.thediary.appium.Gestures;
import br.com.base2.thediary.config.Constants;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.*;

/**
 * Created by base2 on 5/19/17.
 */
public class ModuleBlock extends FluentPage {
    private String moduleName;
    @Page private Actions actions;

    @Step("{method}")
    public void tapAddEntry() {
        FluentWebElement lazyLoader = el(By.xpath("//XCUIElementTypeStaticText[@name='${moduleName}']//following-sibling::XCUIElementTypeButton[@name='Add']".replace(
                "${moduleName}", this.moduleName
        )));

        FluentWebElement lazyLoaderCollection = $(By.xpath("//XCUIElementTypeCollectionView")).last();

        while(lazyLoader.conditions().not().displayed()) {
            int xMiddle = lazyLoaderCollection.size().getWidth() / 2;
            int yMiddle = lazyLoaderCollection.size().getHeight() / 2;

            TouchAction touchAction = new TouchAction((AppiumDriver) getDriver());
            touchAction.press(xMiddle, yMiddle)
                        .moveTo(xMiddle, yMiddle - 500)
                        .release()
                        .perform();
        }

        lazyLoader.click();
    }

    @Step("{method}")
    public void tapListButton() {
        el(By.xpath("//XCUIElementTypeStaticText[@name='${moduleName}']//following-sibling::XCUIElementTypeButton[@name='List']".replace(
            "${moduleName}", this.moduleName
        ))).click();
    }

    @Step("{method}")
    public void tapMoreButton() {
        el(By.xpath("//XCUIElementTypeStaticText[@name='${moduleName}']//following-sibling::XCUIElementTypeButton[@name='More']".replace(
            "${moduleName}", this.moduleName
        ))).click();
    }

    @Step("{method}")
    public void tapChartButton() {
        el(By.xpath("//XCUIElementTypeStaticText[@name='${moduleName}']//following-sibling::XCUIElementTypeButton[@name='Chart']".replace(
            "${moduleName}", this.moduleName
        ))).click();
    }

    @Step("{method}: {0}")
    public void tapSpecificEntry(String entryValue) {
        FluentWebElement lazyLoader = el(By.xpath("//XCUIElementTypeStaticText[contains(@name, '${entryValue}')]".replace("${entryValue}", entryValue)));

        await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS)
               .until(lazyLoader)
               .displayed();

        lazyLoader.click();
    }

    @Step("{method}: {0}")
    public void verifyThatEntryIsLogged(String entryValue) {
        assertThat(actions.get(By.xpath("//XCUIElementTypeStaticText[contains(@value, '${entryValue}')]".replace("${entryValue}", entryValue))).displayed());
    }

    @Step("{method}: {0}")
    public void verifyThatEntryIsNoLongerLogged(String entryValue) {
        assertThat(actions.get(By.xpath("//XCUIElementTypeStaticText[contains(@value, '${entryValue}')]".replace("${entryValue}", entryValue))).conditions().not().displayed());
    }

    public void setModuleName(String moduleName) { this.moduleName = moduleName; }
}
