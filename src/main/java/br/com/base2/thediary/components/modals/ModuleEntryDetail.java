package br.com.base2.thediary.components.modals;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by base2 on 5/23/17.
 */
public class ModuleEntryDetail extends FluentPage {
    @Step("{method}")
    public void tapDeleteButton() {
        el(By.name("DELETE ENTRY")).click();
    }
}
