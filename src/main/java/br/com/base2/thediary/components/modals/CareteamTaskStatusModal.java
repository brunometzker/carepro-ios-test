package br.com.base2.thediary.components.modals;

import org.fluentlenium.core.FluentPage;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by leonardoamaral on 10/03/17.
 */
public class CareteamTaskStatusModal extends FluentPage {
    @Step("{method}")
    public void tapStatusField() {}

    @Step("{method}: {0}")
    public void tapStatusOption(String status) {}
}

