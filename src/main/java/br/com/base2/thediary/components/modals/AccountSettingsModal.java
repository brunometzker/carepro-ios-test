package br.com.base2.thediary.components.modals;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by leonardoamaral on 07/04/17.
 */
public class AccountSettingsModal extends FluentPage {
    @Step("{method}")
    public void tapCancelButton() {
        el(By.name("Cancel")).click();
    }

    @Step("{method}")
    public void tapSaveButton() {
        el(By.name("Save")).click();
    }

    @Step("{method}")
    public void verifyThatModalIsDisplayed() {
        assertThat(el(By.name("Settings")).displayed());
    }
}
