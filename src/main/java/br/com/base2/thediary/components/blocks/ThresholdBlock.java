package br.com.base2.thediary.components.blocks;

import br.com.base2.thediary.appium.ElementStartpoint;
import br.com.base2.thediary.appium.Gestures;
import br.com.base2.thediary.config.Constants;
import io.appium.java_client.AppiumDriver;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.*;

/**
 * Created by leonardoamaral on 15/03/17.
 */
public class ThresholdBlock extends FluentPage {
    @Step("{method}")
    public void tapNewButton() {
        long methodStartTime = System.currentTimeMillis();
        long timeout = 60000;

        FluentWebElement newThreshold = el(By.name("new_threshold_button"));

        while(newThreshold.conditions().not().displayed()) {
            if(System.currentTimeMillis() >= methodStartTime + timeout)
                break;
            else
                Gestures.swipeUp(getDriver(), el(By.xpath("//XCUIElementTypeCollectionView")), ElementStartpoint.MIDDLE, 200);
        }

        await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()))
               .until(newThreshold)
               .displayed();

        newThreshold.click();
    }

    @Step("{method}")
    public void tapRefreshButton() {
        el(By.name("Refresh")).click();
    }

    @Step("{method}")
    public void tapMoreButton() {
        el(By.xpath("//XCUIElementTypeStaticText[@name='THRESHOLDS']//following-sibling::XCUIElementTypeButton[@name='More']")).click();
    }

    @Step("{method}")
    public void tapLessButton() {
        el(By.xpath("//XCUIElementTypeStaticText[@name='THRESHOLDS']/following-sibling::XCUIElementTypeButton[@name='Less']")).click();
    }

    @Step("{method}: {0}")
    public void tapThresholdModuleRecord(String thresholdModule) {
        int tries = 0;

        while(tries <= 3) {
            try {
                System.out.println(((AppiumDriver) getDriver()).getPageSource());
                el(By.xpath("//XCUIElementTypeStaticText[@name='${thresholdModule}']//ancestor::XCUIElementTypeCell".replace("${thresholdModule}", thresholdModule))).click();
            } catch(NoSuchElementException nse) {
                tapRefreshButton();
                try { Thread.sleep(1000); } catch(InterruptedException ie) {}
                tapMoreButton();

            }

            tries++;
        }
    }

    @Step("{method}")
    public void tapEditButton() {
        el(By.name("Edit"));
    }

    @Step("{method}: {0}")
    public void tapThresholdModuleRedIcon(String thresholdModule) {
        el(By.xpath("//XCUIElementTypeButton[contains(@name, 'Delete, ${thresholdModule}')]"
                    .replace("${thresholdModule}", thresholdModule))).click();
    }

    @Step("{method}")
    public void tapDeleteButton() {
        el(By.name("Delete")).click();
    }

    @Step("{method}")
    public void tapDoneButton() {
        el(By.name("Done")).click();
    }

    @Step("{method}")
    public void verifyThatActionButtonsAreDisplayed() {
        assertThat(el(By.name("Edit")));
    }

    @Step("{method}")
    public void verifyThatThereAreActiveThresholds() {
        int tries = 0;
        boolean found = false;

        while(tries <= 3) {
            try {
                if(found)
                    break;
                else
                    found = el(By.xpath("//XCUIElementTypeStaticText[contains(@name, 'thresholds set to trigger alerts')]")).displayed();
            } catch(NoSuchElementException nse) {
                tapRefreshButton();
                try { Thread.sleep(1000); } catch(InterruptedException ie) {}
            }

            tries++;
        }

        assertThat(found == true);
    }

    @Step("{method}")
    public void verifyThatThereAreNoActiveThresholds() {
        assertThat(el(By.id("thresholds_set_to_trigger_alerts_message")).conditions().not().displayed());
    }

    @Step("{method}")
    public void verifyThatDeleteButtonIsDisplayed() {
        assertThat(el(By.name("Delete")).displayed());
    }

    @Step("{method}: {0}")
    public void verifyThatThresholdModuleRecordIsNoLongerDisplayed(String thresholdModule) {
        assertThat(el(By.xpath("//XCUIElementTypeStaticText[@name='${thresholdModule}']//ancestor::XCUIElementTypeCell[1]"
                               .replace("${thresholdModule}", thresholdModule))).conditions().not().displayed());
    }
}