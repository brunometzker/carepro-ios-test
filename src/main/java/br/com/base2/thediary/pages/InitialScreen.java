package br.com.base2.thediary.pages;

import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by bmetzker on 24/02/17.
 */
public class InitialScreen extends FluentPage {
    @Step("{method}")
    public void tapLoginButton() {
        el(By.name("onboard_login")).click();
    }

    @Step("{method}")
    public void verifyThatScreenIsDisplayed() {
        assertThat(el(By.name("Prioritize Care Team Tasks")).displayed());
    }
}
