package br.com.base2.thediary.pages;

import static org.assertj.core.api.Assertions.*;

import br.com.base2.thediary.components.tabbedbar.CareProTabs;
import br.com.base2.thediary.config.Constants;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;
import java.util.concurrent.TimeUnit;

/**
 * Created by bmetzker on 24/02/17.
 */
public class DashboardTab extends FluentPage {
    @Page private CareProTabs careProTabs;

    @Step("{method}")
    public void verifyThatCaregiverIsAtDashboardTab() {
        assertThat(await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(By.id("welcome_label"))).displayed());
    }

    public CareProTabs atCareProTabBar() { return this.careProTabs; }
}
