package br.com.base2.thediary.pages;

import br.com.base2.thediary.components.tabbedbar.CareProTabs;
import br.com.base2.thediary.components.modals.AccountSettingsModal;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by leonardoamaral on 03/03/17.
 */
public class AccountTab extends FluentPage {
    @Page private CareProTabs careProTabs;
    @Page private AccountSettingsModal accountSettingsModal;

    @Step("{method}")
    public void tapOnAccountDetailsButton() {
        el(By.xpath("//XCUIElementTypeStaticText[@name='Account']/following-sibling::XCUIElementTypeButton")).click();
    }

    @Step("{method}")
    public void tapOnLogoutButton() {
        el(By.name("Logout")).click();
    }

    public CareProTabs atCareProTabBar() {
        return this.careProTabs;
    }

    public AccountSettingsModal atAccountSettingsModal() { return this.accountSettingsModal; }
}
