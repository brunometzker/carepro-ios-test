package br.com.base2.thediary.pages;

import br.com.base2.thediary.appium.Actions;
import br.com.base2.thediary.components.modals.*;
import br.com.base2.thediary.components.tabbedbar.CareProTabs;
import br.com.base2.thediary.components.views.OwnerDetails;
import br.com.base2.thediary.config.Constants;
import io.appium.java_client.AppiumDriver;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.concurrent.TimeUnit;

/**
 * Created by leonardoamaral on 02/03/17.
 */
public class OwnersTab extends FluentPage {
    @Page private OwnerDetails ownerDetails;
    @Page private CreateOwnerModal createOwnerModal;
    @Page private CreateCarePlanModal createCarePlanModal;
    @Page private CreateGoalModal createGoalModal;
    @Page private GoalSettingModal goalSettingModal;
    @Page private TaskModal taskModal;
    @Page private CareteamTaskModal careteamTaskModal;
    @Page private CareteamTaskStatusModal careteamTaskStatusModal;
    @Page private EnrollmentTaskModal enrollmentTaskModal;
    @Page private EnrollmentTaskStatusModal enrollmentTaskStatusModal;
    @Page private NewThresholdModal newThresholdModal;
    @Page private CareProTabs careProTabs;
    @Page private NoteModal noteModal;
    @Page private Actions actions;


    @Step("{method}: {0}")
    public void fillSearchFieldWith(String ownerName) {
        FluentWebElement element = actions.get(By.id("owner_search_field"));

        String searchword = null;

        if(ownerName.split(" ").length > 1)
            searchword = ownerName.split(" ")[0];
        else
            searchword = ownerName;

        String text = element.getElement().getText();


        while (element.getElement().getText() != null )
        {
            element.fill().with(String.valueOf(Keys.DELETE));
        }

        element.fill().with(searchword);
    }

    @Step("{method}")
    public void tapOnCancelSearch() { el(By.name("Cancel")).click(); }

    @Step("{method}")
    public void tapAddOwnerButton() { el(By.id("Add Owner Button")).click(); }

    @Step("{method}")
    public void tapClearTextButton() { el(By.name("Clear text")).click(); }

    @Step("{method}: {0}")
    public void verifyThatOwnerRecordIsDisplayed(String ownerName) {
        await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS)
               .until(el(By.xpath("//XCUIElementTypeStaticText[contains(@value, '${ownerName}')]//ancestor::XCUIElementTypeCell[1]"
               .replace("${ownerName}", ownerName)))).displayed();
    }

    @Step("{method}: {0}")
    public void tapOwnerRecord(String ownerName) {
        el(By.xpath("//XCUIElementTypeStaticText[contains(@value, '${ownerName}')]//ancestor::XCUIElementTypeCell[1]"
                    .replace("${ownerName}", ownerName))).click();
    }

    public NoteModal atNoteModal() {return this.noteModal;}

    public OwnerDetails atOwnerDetails() {
        return this.ownerDetails;
    }

    public CreateOwnerModal atCreateOwnerModal() { return this.createOwnerModal; }

    public CareProTabs atCareProTabBar() {
        return this.careProTabs;
    }

    public CreateCarePlanModal atCreateCarePlanModal() {
        return this.createCarePlanModal;
    }

    public CreateGoalModal atCreateGoalModal() { return this.createGoalModal; }

    public GoalSettingModal atGoalSettingModal() { return this.goalSettingModal; }

    public TaskModal atTaskModal() { return this.taskModal; }

    public CareteamTaskModal atCareteamTaskModal() { return this.careteamTaskModal; }

    public CareteamTaskStatusModal atCareteamTaskStatusModal() { return this.careteamTaskStatusModal; }

    public EnrollmentTaskModal atEnrollmentTaskModal() { return this.enrollmentTaskModal; }

    public EnrollmentTaskStatusModal atEnrollmentTaskStatusModal() { return this.enrollmentTaskStatusModal; }

    public NewThresholdModal atNewThresholdModal() { return this.newThresholdModal; }
}