package br.com.base2.thediary.api;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by leonardoamaral on 05/05/17.
 */
public class APIRequests {
    public static String getOwnerBearerToken(String username, String password) {
        MultivaluedMap<String, String> bodyContent = new MultivaluedMapImpl();
        bodyContent.add("grant_type", "");
        bodyContent.add("username", username);
        bodyContent.add("password", password);
        bodyContent.add("client_id", "lhd-ios-client");
        bodyContent.add("client_secret", "mpCZiOUgQCICdGgdvLT0tYXnj6nIsoxHzO6x8Fi6NRw4HqAfdiNHnfUFJY8I9HbOQfPcXRz8OsdJf5cgd5jmOAKfMROSyTdNRI33hZUj8eKhRkVuS7HoJy4OTmWPvQg2");
        bodyContent.add("scope", "openid api");
        bodyContent.add("response_type", "id_token");


        String response = new Client().resource("https://rc-authcenter.ting.ly")
                                      .path("/connect/token")
                                      .type(MediaType.APPLICATION_FORM_URLENCODED)
                                      .post(ClientResponse.class, bodyContent)
                                      .getEntity(String.class);

        Matcher matcher = Pattern.compile("\"access_token\":\"(.+?)\"").matcher(response);
        matcher.find();

        return matcher.group(1);
    }

    public static String getOwnerId(String authorizationToken, String ownerName) {
        String response = new Client().resource("https://rc-api.ting.ly")
                                      .path("/api/1/CarePro/Patient/GetPaged")
                                      .queryParam("pageLength", "1000")
                                      .queryParam("pageNumber", "1")
                                      .header("Authorization", "Bearer " + authorizationToken)
                                      .get(ClientResponse.class)
                                      .getEntity(String.class);

        Matcher matcher = Pattern.compile("\"PatientId\":(\\d+),\"UID\":\"[^\"]+\",\"Email\":\"[^\"]+\",\"FirstName\":\"" + ownerName + "\"").matcher(response);
        matcher.find();

        return matcher.group(1);
    }

    public static String getProblemId(String authorizationToken, String ownerId, String problem) {
        String response = new Client().resource("https://rc-api.ting.ly")
                                      .path("/api/1/CarePro/CarePlan/" + ownerId + "/GoalSetting")
                                      .header("Authorization", "Bearer " + authorizationToken)
                                      .get(ClientResponse.class)
                                      .getEntity(String.class);

        Matcher matcher = Pattern.compile("\"Id\":(.+?),\"CarePlanId\":(.+?),\"AssignedStaffMemberId\":(.+?),\"Goal\":\"(.+?)\",\"Problem\":\"" + problem + "\"").matcher(response);
        matcher.find();

        return matcher.group(1);
    }
}
