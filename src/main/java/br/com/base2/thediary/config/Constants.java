package br.com.base2.thediary.config;

/**
 * Created by leonardoamaral on 28/03/17.
 */
public enum Constants {
    DEFAULT_ELEMENT_TIMEOUT("60");

    private String value;

    Constants(String value) {
        this.value = value;
    }

    public String getValue() { return this.value; }
}
