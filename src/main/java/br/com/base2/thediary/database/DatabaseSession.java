package br.com.base2.thediary.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by leonardoamaral on 07/04/17.
 */
public class DatabaseSession {
    public static class Builder {
        private String jdbcConnString = "jdbc:sqlserver://${serverName}:${serverPort};" +
                                        "databaseName=${databaseName};" +
                                        "user=${username};" +
                                        "password=${password};";

        public Builder onServer(String serverName) {
            this.jdbcConnString = this.jdbcConnString.replace("${serverName}", serverName);

            return this;
        }

        public Builder throughPort(String portNumber) {
            this.jdbcConnString = this.jdbcConnString.replace("${serverPort}", portNumber);

            return this;
        }

        public Builder onDatabase(String databaseName) {
            this.jdbcConnString = this.jdbcConnString.replace("${databaseName}", databaseName);

            return this;
        }

        public Builder usingUsername(String username) {
            this.jdbcConnString = this.jdbcConnString.replace("${username}", username);

            return this;
        }

        public Builder usingPassword(String password) {
            this.jdbcConnString = this.jdbcConnString.replace("${password}", password);

            return this;
        }

        public Connection build() {
            try {
                return DriverManager.getConnection(jdbcConnString);
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
