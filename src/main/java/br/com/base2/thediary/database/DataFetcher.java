package br.com.base2.thediary.database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by leonardoamaral on 09/05/17.
 */
public class DataFetcher {
    public static Map<String, List<String>> getResultSetAsMap(ResultSet resultSet) {
        Map<String, List<String>> map = new HashMap<String, List<String>>();

        try {
            ResultSetMetaData metaData = resultSet.getMetaData();

            for(int i = 1; i <= metaData.getColumnCount(); i++) {
                map.put(metaData.getColumnName(i), new ArrayList<String>());
            }

            while(resultSet.next()) {
                for(int i = 1; i <= metaData.getColumnCount(); i++) {
                    map.get(metaData.getColumnName(i)).add(resultSet.getString(metaData.getColumnName(i)));
                }
            }
        } catch(SQLException sql) {
            sql.printStackTrace();
        }

        return map;
    }

    public static List<String> getColumnData(ResultSet resultSet, String columnName) {
        List<String> list = new ArrayList<String>();

        try {
            while(resultSet.next()) {
                list.add(resultSet.getString(columnName));
            }

            return list;
        } catch(SQLException sql) {
            sql.printStackTrace();
        }

        return list;
    }

    public static int getResultSetSize(ResultSet resultSet) {
        int counter = 0;

        try {
            while(resultSet.next())
                counter++;

            return counter;
        } catch(SQLException sql) {
            sql.printStackTrace();
        }

        return -1;
    }
}
