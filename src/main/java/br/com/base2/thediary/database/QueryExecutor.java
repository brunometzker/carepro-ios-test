package br.com.base2.thediary.database;

import com.microsoft.sqlserver.jdbc.SQLServerStatement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by leonardoamaral on 09/05/17.
 */
public class QueryExecutor implements CRUD {
    private String sqlStatement;

    public QueryExecutor(String sqlStatement) {
        this.sqlStatement = sqlStatement;
    }

    @Override
    public ResultSet performQuery(Connection connection) {
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(sqlStatement);
            return statement.executeQuery();
        } catch(SQLException sql) {
            sql.printStackTrace();
        }

        return null;
    }

    @Override
    public int performUpdate(Connection connection) {
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(sqlStatement);
            return statement.executeUpdate();
        } catch(SQLException sql) {
            sql.printStackTrace();
        }

        return -1;
    }
}
