package br.com.base2.thediary.database;

import java.sql.Connection;
import java.sql.ResultSet;

/**
 * Created by leonardoamaral on 09/05/17.
 */
public interface CRUD {
    ResultSet performQuery(Connection connection);
    int performUpdate(Connection connection);
}
