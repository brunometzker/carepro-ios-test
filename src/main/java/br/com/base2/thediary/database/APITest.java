package br.com.base2.thediary.database;

import br.com.base2.thediary.api.APIRequests;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.junit.Test;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by leonardoamaral on 04/05/17.
 */
public class APITest {
    @Test
    public void test() {
        String bearerToken = APIRequests.getOwnerBearerToken("autotest@thediary.com", "Healthy15");
        String ownerId = APIRequests.getOwnerId(bearerToken, "MarlonP2");

        System.out.println(ownerId);

        System.out.println(APIRequests.getProblemId(bearerToken, ownerId, "Overweight"));
    }
}
