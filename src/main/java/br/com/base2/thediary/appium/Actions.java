package br.com.base2.thediary.appium;

import br.com.base2.thediary.config.Constants;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.domain.FluentWebElement;
import org.fluentlenium.core.wait.FluentWait;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;

import java.util.concurrent.TimeUnit;

public class Actions extends FluentPage {


    public void tap(By element) {
        try {
            await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(element));
            el(element).click();
        } catch (TimeoutException ex) {
            await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(element));
            el(element).click();
        } catch (NoSuchElementException ex) {
            await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(element));
            el(element).click();
        } catch (Exception ex) {
            getDriver().quit();
        } finally {
            System.out.println("Tapping -> " + element);
        }
    }

    public void fill(By element, String content) {
        try {
            await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(element));
            el(element).fill().with(content);
        } catch (TimeoutException ex) {
            await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(element));
            el(element).fill().with(content);
        } catch (NoSuchElementException ex) {
            await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(element));
            el(element).fill().with(content);
        } catch (Exception ex) {
            getDriver().quit();
        } finally {
            System.out.println("Filling -> " + element + " with =>" + content);
        }
    }


    public FluentWebElement get(By element) {

        try {
            await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(element));
            return el(element);
        } catch (TimeoutException ex) {
            await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(element));
            return el(element);
        } catch (NoSuchElementException ex) {
            await().atMost(Integer.valueOf(Constants.DEFAULT_ELEMENT_TIMEOUT.getValue()), TimeUnit.SECONDS).until(el(element));
            return el(element);
        } catch (Exception ex) {
            ex.printStackTrace();
            getDriver().quit();
        } finally {
            System.out.println("Getting -> " + element);
        }
        return null;
    }
}

