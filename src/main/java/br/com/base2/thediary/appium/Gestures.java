package br.com.base2.thediary.appium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/**
 * Created by leonardoamaral on 24/03/17.
 */
public class Gestures {
    public static void swipeDown(WebDriver webDriver, FluentWebElement from, ElementStartpoint elementStartpoint, int yOffset) {
        AppiumDriver appiumDriver = (AppiumDriver) webDriver;
        TouchAction touchAction = new TouchAction(appiumDriver);

        WebElement webElement = from.getElement();

        int startpointX = 0;
        int startpointY = 0;


        switch(elementStartpoint) {
            case MIDDLE:
                startpointX = webElement.getLocation().getX() + webElement.getSize().getWidth() / 2;
                startpointY = webElement.getLocation().getY() + webElement.getSize().getHeight() / 2;
                break;
            case LEFT:
                startpointX = webElement.getLocation().getX();
                startpointY = webElement.getLocation().getY();
                break;
            case RIGHT:
                startpointX = webElement.getSize().getWidth() + webElement.getLocation().getX();
                startpointY = webElement.getSize().getHeight() + webElement.getLocation().getY();
                break;
        }

        touchAction.press(startpointX, startpointY)
                .waitAction(500)
                .moveTo(startpointX, startpointY - yOffset)
                .release();

        touchAction.perform();
    }

    public static void swipeUp(WebDriver webDriver, FluentWebElement from, ElementStartpoint elementStartpoint, int yOffset) {
        AppiumDriver appiumDriver = (AppiumDriver) webDriver;
        TouchAction touchAction = new TouchAction(appiumDriver);

        WebElement webElement = from.getElement();

        int startpointX = 0;
        int startpointY = 0;

        switch(elementStartpoint) {
            case MIDDLE:
                startpointX = webElement.getLocation().getX() + webElement.getSize().getWidth() / 2;
                startpointY = webElement.getLocation().getY() + webElement.getSize().getHeight() / 2;
                break;
            case LEFT:
                startpointX = webElement.getLocation().getX();
                startpointY = webElement.getLocation().getY();
                break;
            case RIGHT:
                startpointX = webElement.getSize().getWidth() + webElement.getLocation().getX();
                startpointY = webElement.getSize().getHeight() + webElement.getLocation().getY();
                break;
        }

        touchAction.press(startpointX, startpointY)
                   .waitAction(500)
                   .moveTo(startpointX, -yOffset)
                   .release();

        touchAction.perform();
    }
}
