package br.com.base2.thediary.appium;

/**
 * Created by bmetzker on 30/05/17.
 */
public enum ElementStartpoint {
    MIDDLE, RIGHT, LEFT;
}
