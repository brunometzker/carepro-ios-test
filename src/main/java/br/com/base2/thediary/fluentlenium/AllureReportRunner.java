package br.com.base2.thediary.fluentlenium;

import io.appium.java_client.AppiumDriver;
import org.fluentlenium.adapter.junit.FluentTest;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;

/**
 * Created by leonardoamaral on 16/05/17.
 */
public class AllureReportRunner extends FluentTest {
    @Rule
    public TestWatcher screenshotOnFailure = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
            attachScreenshot();
        }

        @Attachment("{method}")
        private byte[] attachScreenshot() {
            return ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
        }
    };
}