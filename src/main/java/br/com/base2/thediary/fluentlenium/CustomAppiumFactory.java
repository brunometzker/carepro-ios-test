package br.com.base2.thediary.fluentlenium;

import io.appium.java_client.AppiumDriver;
import org.fluentlenium.configuration.ConfigurationProperties;
import org.fluentlenium.configuration.FactoryName;
import org.fluentlenium.configuration.WebDriverFactory;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;

/**
 * Created by bmetzker on 24/02/17.
 */
@FactoryName("appium")
public class CustomAppiumFactory implements WebDriverFactory {
    public WebDriver newWebDriver(Capabilities capabilities, ConfigurationProperties configurationProperties) {
        return new AppiumDriver(capabilities);
    }
}
