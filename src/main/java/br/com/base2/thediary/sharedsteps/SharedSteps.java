package br.com.base2.thediary.sharedsteps;

import br.com.base2.thediary.pages.AccountTab;
import br.com.base2.thediary.pages.DashboardTab;
import br.com.base2.thediary.pages.InitialScreen;
import br.com.base2.thediary.components.modals.LoginModal;
import br.com.base2.thediary.pages.OwnersTab;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.annotation.Page;

import java.util.Random;
import java.util.UUID;

/**
 * Created by leonardoamaral on 03/03/17.
 */
public class SharedSteps extends FluentPage {
    @Page private InitialScreen initialScreen;
    @Page private LoginModal loginModal;
    @Page private DashboardTab dashboardTab;
    @Page private AccountTab accountsTab;
    @Page private OwnersTab ownersTab;

    public void login(String username, String password) {
        initialScreen.tapLoginButton();
        loginModal.fillUsernameWith(username);
        loginModal.fillPasswordWith(password);
        loginModal.submitCredentials();
        dashboardTab.verifyThatCaregiverIsAtDashboardTab();
    }

    public void createNewOwner(String ownerName, String ownerSurname, String ownerGender, String ownerPatientGroup, String ownerCareManager) {
        ownersTab.atCreateOwnerModal().fillFirstNameField(ownerName);
        ownersTab.atCreateOwnerModal().fillSurnameField(ownerSurname);
        ownersTab.atCreateOwnerModal().selectGender(ownerGender);
        ownersTab.atCreateOwnerModal().selectPatientGroup(ownerPatientGroup);
        ownersTab.atCreateOwnerModal().selectCareManager(ownerCareManager);
        ownersTab.atCreateOwnerModal().tapSaveButton();
    }

    public void selectOwnerRecord(String ownerName) {
        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.verifyThatOwnerRecordIsDisplayed(ownerName);
        ownersTab.tapOwnerRecord(ownerName);
    }

    public void returnToKnownState() {
        dashboardTab.atCareProTabBar().goToDashboard();
    }
}
