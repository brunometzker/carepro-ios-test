import requests
import sys
import json
import urllib
import time

build_number = sys.argv[1]
build_timestamp = int(time.time())

json_url = "http://localhost:8080/job/CarePro-Test/%s/testReport/api/json" % build_number

try:
    json = requests.get(json_url, auth=("admin", "Healthy15")).json()
except:
    raise Exception("Build number: %s does not exist" % build_number)

executed = json['totalCount']
failed = json['failCount']
passed = json['totalCount'] - json['failCount'] - json['skipCount']
skipped = json['skipCount']
environment = 'RC'

if(int(failed) > 0):
    execution_status_color = "#FF0000"
else:
    execution_status_color = "#008000"

team_token = 'xoxp-8115119047-141289943762-157485433457-7552b0e85410bb390fb3239d61859350'
team_channel = 'ci_builds'
text = "iOS Automation - Slack Bot"
attachments = "[{\"fallback\": \"Test Execution #%s - Build Overview\", \"text\": \"<http://208.52.190.152:8080/job/CarePro-Test/%s/|Test Execution #%s> - Build Overview\", \"fields\": [{\"title\": \"# Executed\", \"value\": \"%s\", \"short\": True}, {\"title\": \"# Passed\", \"value\": \"%s\", \"short\": True}, {\"title\": \"# Failed\", \"value\": \"%s\", \"short\": True}, {\"title\": \"# Skipped\", \"value\": \"%s\", \"short\": True}, {\"title\": \"Environment\", \"value\": \"%s\", \"short\": True}, {\"title\": \"Execution Date\", \"value\": \"<!date^%s^{date_short} {time}|Unable to retrive execution date>\", \"short\": True }, {\"title\": \"Allure Report\", \"value\": \"<http://208.52.190.152:8080/job/CarePro-Test/%s/allure/|Open>\", \"short\": True }], \"color\": \"%s\"}]" % (build_number, build_number, build_number, executed, passed, failed, skipped, environment, build_timestamp, build_number, execution_status_color)
jenkins_icon_url = "https://jenkins.io/images/226px-Jenkins_logo.svg.png"

url_params = {"token" : team_token, "channel": team_channel, "text": text, "attachments": attachments, "icon_url": jenkins_icon_url}

response = requests.get("https://slack.com/api/chat.postMessage", params=url_params)
