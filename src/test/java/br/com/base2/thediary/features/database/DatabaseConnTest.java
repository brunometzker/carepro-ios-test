package br.com.base2.thediary.features.database;

import br.com.base2.thediary.database.DataFetcher;
import br.com.base2.thediary.database.DatabaseSession;
import br.com.base2.thediary.database.QueryExecutor;
import org.junit.Test;
import sun.util.resources.cldr.ka.CalendarData_ka_GE;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by leonardoamaral on 07/04/17.
 */
public class DatabaseConnTest {

    public void databaseConnTest() throws SQLException {
        String serverName = "192.168.200.129";
        String serverPort = "1433";
        String databaseName = "ReleaseCandidate";
        String username = "bruno";
        String password = "Vagabond1";

        Connection conn = new DatabaseSession.Builder()
                                             .onServer(serverName)
                                             .throughPort(serverPort)
                                             .onDatabase(databaseName)
                                             .usingUsername(username)
                                             .usingPassword(password)
                                             .build();

        QueryExecutor executor = new QueryExecutor("SELECT * FROM ModuleAspect");
        ResultSet resultSet = executor.performQuery(conn);

        DataFetcher.getResultSetAsMap(resultSet).get("Name").forEach((entry) -> System.out.println(entry));
    }
}
