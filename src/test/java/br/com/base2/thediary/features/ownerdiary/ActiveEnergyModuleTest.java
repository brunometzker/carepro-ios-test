package br.com.base2.thediary.features.ownerdiary;

import br.com.base2.thediary.appium.Actions;
import br.com.base2.thediary.components.modals.ModuleEntryDetail;
import br.com.base2.thediary.components.modals.ModuleEntryModal;
import br.com.base2.thediary.fluentlenium.AllureReportRunner;
import br.com.base2.thediary.pages.DashboardTab;
import br.com.base2.thediary.pages.InitialScreen;
import br.com.base2.thediary.pages.OwnersTab;
import br.com.base2.thediary.sharedsteps.SharedSteps;
import br.com.base2.thediary.utils.Utilities;
import jersey.repackaged.com.google.common.collect.ImmutableMap;
import org.fluentlenium.core.annotation.Page;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by base2 on 5/19/17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Features("CRUD Active Energy Module Owner Diary")
public class ActiveEnergyModuleTest extends AllureReportRunner{
    @Page private SharedSteps sharedSteps;
    @Page private DashboardTab dashboardTab;
    @Page private OwnersTab ownersTab;
    @Page private ModuleEntryModal moduleEntryModal;
    @Page private ModuleEntryDetail moduleEntryDetail;

    private String username = "autotest@thediary.com";
    private String password = "Healthy15";
    private String ownerName = "MarlonP2 2ndPatient";
    private String moduleBlock = "ACTIVE ENERGY";
    private final static String entryValue = Utilities.generateRandomNumber();

    @Test
    @Stories("Add new Active Energy Module Entry")
    public void testA_addActiveEnergyEntry() {
        sharedSteps.login(username, password);

        dashboardTab.atCareProTabBar().goToOwners();

        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.tapOwnerRecord(ownerName);
        ownersTab.tapOnCancelSearch();
        ownersTab.atOwnerDetails().tapOnDiarySection();
        ownersTab.atOwnerDetails().atDiarySection().atModuleBlock(moduleBlock).tapAddEntry();

        moduleEntryModal.fillEntryValue(entryValue);
        moduleEntryModal.tapSaveButton();

        ownersTab.atOwnerDetails().atDiarySection().atModuleBlock(moduleBlock).tapListButton();
        ownersTab.atOwnerDetails().atDiarySection().atModuleBlock(moduleBlock).verifyThatEntryIsLogged(entryValue);
    }

    @Test
    @Stories("Delete Active Energy Module Entry")
    public void testB_deleteActiveEnergyEntry() {
        ownersTab.atOwnerDetails().atDiarySection().atModuleBlock(moduleBlock).tapSpecificEntry(entryValue);

        moduleEntryDetail.tapDeleteButton();

        ownersTab.atOwnerDetails().atDiarySection().atModuleBlock(moduleBlock).tapListButton();
        ownersTab.atOwnerDetails().atDiarySection().atModuleBlock(moduleBlock).verifyThatEntryIsNoLongerLogged(entryValue);
    }
}