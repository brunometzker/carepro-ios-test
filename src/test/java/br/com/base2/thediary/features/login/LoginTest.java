package br.com.base2.thediary.features.login;

import br.com.base2.thediary.components.modals.LoginModal;
import br.com.base2.thediary.fluentlenium.AllureReportRunner;
import br.com.base2.thediary.pages.AccountTab;
import br.com.base2.thediary.pages.DashboardTab;
import br.com.base2.thediary.pages.InitialScreen;
import br.com.base2.thediary.sharedsteps.SharedSteps;
import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.annotation.Page;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.MethodSorters;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static junit.framework.TestCase.fail;

/**
 * Created by leonardoamaral on 10/03/17.
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Features("Login")
public class LoginTest extends AllureReportRunner {
    @Page
    private AccountTab accountTab;
    @Page
    private InitialScreen initialScreen;
    @Page
    private LoginModal loginModal;
    @Page
    private DashboardTab dashboardTab;

    @Test
    @Stories("Login as CarePro Staff successfully")
    public void testA_loginInAsCareProStaff() {
        String login = "lhdtester+org1masteradmin@gmail.com";
        String password = "Vagabond1";

        initialScreen.tapLoginButton();
        loginModal.fillUsernameWith(login);
        loginModal.fillPasswordWith(password);
        loginModal.submitCredentials();

        dashboardTab.verifyThatCaregiverIsAtDashboardTab();

        dashboardTab.atCareProTabBar().goToAccount();
        accountTab.tapOnAccountDetailsButton();
        accountTab.tapOnLogoutButton();
    }

    @Test
    @Stories("Login as CarePro Chief Org successfully")
    public void testB_loginAsCareProChiefOrg() {
        String login = "lhdtester+org2staffadmin@gmail.com";
        String password = "Vagabond1";

        initialScreen.tapLoginButton();
        loginModal.fillUsernameWith(login);
        loginModal.fillPasswordWith(password);
        loginModal.submitCredentials();

        dashboardTab.verifyThatCaregiverIsAtDashboardTab();

        dashboardTab.atCareProTabBar().goToAccount();
        accountTab.tapOnAccountDetailsButton();
        accountTab.tapOnLogoutButton();
    }

    @Test
    @Stories("Login as CarePro Patient Admin successfully")
    public void testC_loginAsCareProPatientAdmin() {
        String login = "lhdtester+org1patientadmin1@gmail.com";
        String password = "Vagabond1";

        initialScreen.tapLoginButton();
        loginModal.fillUsernameWith(login);
        loginModal.fillPasswordWith(password);
        loginModal.submitCredentials();

        dashboardTab.verifyThatCaregiverIsAtDashboardTab();

        dashboardTab.atCareProTabBar().goToAccount();
        accountTab.tapOnAccountDetailsButton();
        accountTab.tapOnLogoutButton();
    }

    @Test
    @Stories("Login as CarePro Reports Admin successfully")
    public void testD_loginAsCareProReportsAdmin() {
        String login = "lhdtester+org1reportadmin@gmail.com";
        String password = "Vagabond1";

        initialScreen.tapLoginButton();
        loginModal.fillUsernameWith(login);
        loginModal.fillPasswordWith(password);
        loginModal.submitCredentials();

        dashboardTab.verifyThatCaregiverIsAtDashboardTab();

        dashboardTab.atCareProTabBar().goToAccount();
        accountTab.tapOnAccountDetailsButton();
        accountTab.tapOnLogoutButton();
    }

    @Test
    @Stories("Login as CarePro System Admin successfully")
    public void testE_loginAsCareProSystemAdmin() {
        String login = "lhdtester+org1staffadmin@gmail.com";
        String password = "Vagabond1";

        initialScreen.tapLoginButton();
        loginModal.fillUsernameWith(login);
        loginModal.fillPasswordWith(password);
        loginModal.submitCredentials();

        dashboardTab.verifyThatCaregiverIsAtDashboardTab();

        dashboardTab.atCareProTabBar().goToAccount();
        accountTab.tapOnAccountDetailsButton();
        accountTab.tapOnLogoutButton();
    }

    @Test
    @Stories("Login with invalid username")
    public void testF_loginWithInvalidUsername() {
        String login = "lhdtester";
        String password = "Healthy15";

        initialScreen.tapLoginButton();
        loginModal.fillUsernameWith(login);
        loginModal.fillPasswordWith(password);
        loginModal.submitCredentials();
        loginModal.verifyThatErrorMessageIs("Invalid Login");

        loginModal.closeModal();
    }

    @Test
    @Stories("Login with empty username")
    public void testG_loginWithEmptyUsername() {
        String login = "";
        String password = "Healthy15";

        initialScreen.tapLoginButton();
        loginModal.fillUsernameWith(login);
        loginModal.fillPasswordWith(password);
        loginModal.submitCredentials();
        loginModal.verifyThatErrorMessageIs("Email is required");

        loginModal.closeModal();
    }

    @Test
    @Stories("Login with invalid password")
    public void testH_loginWithInvalidPassword() {
        String login = "autotest@thediary.com";
        String password = "Vagabond1";

        initialScreen.tapLoginButton();
        loginModal.fillUsernameWith(login);
        loginModal.fillPasswordWith(password);
        loginModal.submitCredentials();
        loginModal.verifyThatErrorMessageIs("Invalid Login");

        loginModal.closeModal();
    }

    @Test
    @Stories("Login with empty password")
    public void testI_loginWithEmptyPassword() {
        String login = "autotest@thediary.com";
        String password = "";

        initialScreen.tapLoginButton();
        loginModal.fillUsernameWith(login);
        loginModal.fillPasswordWith(password);
        loginModal.submitCredentials();
        loginModal.verifyThatErrorMessageIs("Password is required");
    }
}