package br.com.base2.thediary.features.thresholds;

import br.com.base2.thediary.fluentlenium.AllureReportRunner;
import br.com.base2.thediary.pages.DashboardTab;
import br.com.base2.thediary.pages.OwnersTab;
import br.com.base2.thediary.sharedsteps.SharedSteps;
import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.annotation.Page;
import org.junit.After;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Created by leonardoamaral on 22/03/17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Features("Blood Pressure Thresholds")
public class BloodPressureThresholdTest extends AllureReportRunner {
    private String login = "autotest@thediary.com";
    private String password = "Healthy15";

    @Page private SharedSteps sharedSteps;
    @Page private DashboardTab dashboardTab;
    @Page private OwnersTab ownersTab;

    @Test
    @Stories("Tap new button to create blood pressure threshold")
    public void testA_tapNewButtonToCreateBloodPressureThreshold() {
        String ownerName = "MarlonP2";

        sharedSteps.login(login, password);

        dashboardTab.atCareProTabBar().goToOwners();
        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.tapOwnerRecord(ownerName);
        ownersTab.tapOnCancelSearch();
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapNewButton();

        ownersTab.atNewThresholdModal().verifyThatModalIsDisplayed();
    }

    /*
    @Ignore
    @Test
    @Stories("Fill in threshold details for blood pressure")
    public void testB() throws InterruptedException {
        String thresholdModule = "Blood Pressure";
        String notificationMethod = "Email";
        String systolicMin = "10";
        String systolicMax = "20";
        String diastolicMin = "10";
        String diastolicMax = "20";

        ownersTab.atNewThresholdModal().selectThresholdModule(thresholdModule);
        ownersTab.atNewThresholdModal().selectNotificationMethod(notificationMethod);

        ownersTab.atNewThresholdModal().fillInSystolicMinimum(systolicMin);
        ownersTab.atNewThresholdModal().fillInDiastolicMinimun(diastolicMin);
        ownersTab.atNewThresholdModal().fillInSystolicMaximum(systolicMax);
        ownersTab.atNewThresholdModal().fillInDiastolicMaximum(diastolicMax);

        ownersTab.atNewThresholdModal().tapSaveButton();

        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().verifyThatThereAreActiveThresholds();
    }

    @Ignore
    @Test
    @Stories("Tap more to expand threshold view")
    public void testC() {
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapMoreButton();
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().verifyThatActionButtonsAreDisplayed();
    }

    @Ignore
    @Test
    @Stories("Click on blood pressure threshold to edit details")
    public void testE() {}

    @Ignore
    @Test
    @Stories("Tap edit, then red icon next to the threshold you want to delete")
    public void testF() {}

    @Ignore
    @Test
    @Stories("Tap done")
    public void testD() {}

    @Ignore
    @Test
    @Stories("Tap less to collapse threshold view")
    public void testG() {} */
}
