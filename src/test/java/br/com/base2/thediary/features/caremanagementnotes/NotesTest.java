package br.com.base2.thediary.features.caremanagementnotes;

import br.com.base2.thediary.components.modals.ModuleEntryDetail;
import br.com.base2.thediary.components.modals.ModuleEntryModal;
import br.com.base2.thediary.fluentlenium.AllureReportRunner;
import br.com.base2.thediary.pages.DashboardTab;
import br.com.base2.thediary.pages.OwnersTab;
import br.com.base2.thediary.sharedsteps.SharedSteps;
import br.com.base2.thediary.utils.Utilities;
import org.fluentlenium.core.annotation.Page;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Created by base2 on 5/19/17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Features("Care Management Notes")
public class NotesTest extends AllureReportRunner {
    @Page private SharedSteps sharedSteps;
    @Page private DashboardTab dashboardTab;
    @Page private OwnersTab ownersTab;
    @Page private ModuleEntryModal moduleEntryModal;
    @Page private ModuleEntryDetail moduleEntryDetail;

    private String username = "autotest@thediary.com";
    private String password = "Healthy15";
    private String ownerName = "MarlonP2 2ndPatient";
    private final static String careManagementTime = Utilities.generateRandomNumber(200);
    private final static String subject = Utilities.generateRandomAlphanumericString();
    private final static String note = "iOS automation - " + Utilities.generateRandomAlphanumericString();

    @Test
    @Stories("Create note via patient dashboard")
    public void testA_createNoteViaPatientDashboard() {
        sharedSteps.login(username, password);

        dashboardTab.atCareProTabBar().goToOwners();

        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.tapOwnerRecord(ownerName);
        ownersTab.tapOnCancelSearch();

        ownersTab.atOwnerDetails().tapAddNoteButton();
        ownersTab.atNoteModal().fillCareManagementTime(careManagementTime);
        ownersTab.atNoteModal().fillSubject(subject);
        ownersTab.atNoteModal().fillNote(note);
        ownersTab.atNoteModal().tapSaveButton();

        ownersTab.atOwnerDetails().atDashboardSection().atNotesBlock().verifyThatNoteIsDisplayed(note);

        ownersTab.atOwnerDetails().atDashboardSection().atNotesBlock().tapOpenNote(note);

        ownersTab.atNoteModal().verifyCareManagementTime(careManagementTime);
        ownersTab.atNoteModal().verifySubject(subject);
        ownersTab.atNoteModal().verifyNote(note);
    }

}