package br.com.base2.thediary.features.owners;

import br.com.base2.thediary.components.modals.CreateOwnerModal;
import br.com.base2.thediary.components.modals.LoginModal;
import br.com.base2.thediary.fluentlenium.AllureReportRunner;
import br.com.base2.thediary.pages.DashboardTab;
import br.com.base2.thediary.pages.InitialScreen;
import br.com.base2.thediary.pages.OwnersTab;
import br.com.base2.thediary.sharedsteps.SharedSteps;
import br.com.base2.thediary.utils.Utilities;
import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.annotation.Page;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Created by leonardoamaral on 10/04/17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Features("Owners")
public class OwnersTest extends AllureReportRunner {
    private String ownerName = "MarlonP2 2ndPatient";

    @Page private InitialScreen initialScreen;
    @Page private LoginModal loginModal;
    @Page private DashboardTab dashboardTab;
    @Page private OwnersTab ownersTab;
    @Page private CreateOwnerModal createOwnerModal;
    @Page private SharedSteps sharedSteps;

    @Test
    @Stories("Search for specific owner on 'Find Owner' search bar")
    public void testA_searchSpecificOwner() {
        String login = "autotest@thediary.com";
        String password = "Healthy15";

        initialScreen.tapLoginButton();

        loginModal.fillUsernameWith(login);
        loginModal.fillPasswordWith(password);
        loginModal.submitCredentials();

        dashboardTab.verifyThatCaregiverIsAtDashboardTab();
        dashboardTab.atCareProTabBar().goToOwners();

        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.verifyThatOwnerRecordIsDisplayed(ownerName);
    }

    @Test
    @Stories("Filter owners by last name")
    public void testB_filterOwnersByLastName() {
        ownersTab.tapClearTextButton();
        ownersTab.fillSearchFieldWith(ownerName.split(" ")[1]);

        ownersTab.verifyThatOwnerRecordIsDisplayed(ownerName);
    }

    @Test
    @Stories("Filter owners by first name")
    public void testC_filterOwnersByFirstName() {
        ownersTab.tapClearTextButton();
        ownersTab.fillSearchFieldWith(ownerName.split(" ")[0]);

        ownersTab.verifyThatOwnerRecordIsDisplayed(ownerName);
    }

    @Test
    @Stories("Check information displayed in owner's banner")
    public void testD_checkInfoDisplayedAtOwnerBanner() {
        ownersTab.tapOnCancelSearch();
        ownersTab.tapOwnerRecord(ownerName);
        ownersTab.atOwnerDetails().verifyThatBannerInformationIsDisplayed(ownerName);
    }

    @Test
    @Stories("Open and save an empty Create Owner form")
    public void testE_openAndSaveEmptyCreateOwnerForm() {
        ownersTab.tapAddOwnerButton();

        createOwnerModal.tapSaveButton();
        createOwnerModal.verifyThatRequiredFieldsMessageWasDisplayed();
    }

    @Test
    @Stories("Create new owner")
    public void testF_createNewOwner() {

        String newOwnerFirstName = Utilities.generateRandomAlphanumericString();
        String newOwnerLastName = Utilities.generateRandomAlphanumericString();
        String newOwnerGroup = "Default Patient Group";
        String newOwnerCareManager = "Autotest Marlon";

        createOwnerModal.fillFirstNameField(newOwnerFirstName);
        createOwnerModal.fillSurnameField(newOwnerLastName);
        createOwnerModal.selectPatientGroup(newOwnerGroup);
        createOwnerModal.selectCareManager(newOwnerCareManager);
        createOwnerModal.tapSaveButton();

        ownersTab.tapClearTextButton();
        ownersTab.fillSearchFieldWith(newOwnerFirstName);
        ownersTab.verifyThatOwnerRecordIsDisplayed(newOwnerFirstName);
    }
}
