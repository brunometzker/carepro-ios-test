package br.com.base2.thediary.features.thresholds;

import br.com.base2.thediary.fluentlenium.AllureReportRunner;
import br.com.base2.thediary.pages.DashboardTab;
import br.com.base2.thediary.pages.OwnersTab;
import br.com.base2.thediary.sharedsteps.SharedSteps;
import org.fluentlenium.core.annotation.Page;
import org.junit.*;
import org.junit.runners.MethodSorters;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Created by leonardoamaral on 15/03/17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Features("Blood Glucose Threshold")
public class BloodGlucoseThresholdTest extends AllureReportRunner {
    private String thresholdModule = "Blood Glucose";
    private String notificationMethod = "Email";
    private String minimumLimit = "10";
    private String maximumLimit = "20";

    @Page private SharedSteps sharedSteps;
    @Page private DashboardTab dashboardTab;
    @Page private OwnersTab ownersTab;

    @Test
    @Stories("Tap new button to create blood glucose threshold")
    public void testA_tapNewButtonToCreateBloodGlucoseThreshold() {
        String ownerName = "MarlonP2";

        sharedSteps.login("autotest@thediary.com", "Healthy15");

        dashboardTab.atCareProTabBar().goToOwners();
        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.tapOwnerRecord(ownerName);
        ownersTab.tapOnCancelSearch();
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapNewButton();

        ownersTab.atNewThresholdModal().verifyThatModalIsDisplayed();
    }

    /* @Ignore
    @Test
    @Stories("Fill in threshold details for blood glucose")
    public void testB() {
        ownersTab.atNewThresholdModal().selectThresholdModule(thresholdModule);
        ownersTab.atNewThresholdModal().selectNotificationMethod(notificationMethod);
        ownersTab.atNewThresholdModal().fillInMinimunLimit(minimumLimit);
        ownersTab.atNewThresholdModal().fillInMaximumLimit(maximumLimit);
        ownersTab.atNewThresholdModal().tapSaveButton();

        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().verifyThatThereAreActiveThresholds();
    }

    @Ignore
    @Test
    @Stories("Tap more to expand threshold view")
    public void testC() {
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapMoreButton();
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().verifyThatActionButtonsAreDisplayed();
    }

    @Ignore
    @Test
    @Stories("Click on blood glucose threshold to edit details")
    public void testD() {
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapThresholdModuleRecord(thresholdModule);
        ownersTab.atNewThresholdModal().verifyThatModalIsDisplayed();

        ownersTab.atNewThresholdModal().tapCancelButton();
    }

    @Ignore
    @Test
    @Stories("Tap edit, then red icon next to the threshold you want to delete")
    public void testE() {
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapEditButton();
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapThresholdModuleRedIcon(thresholdModule);

        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().verifyThatDeleteButtonIsDisplayed();
    }

    @Ignore
    @Test
    @Stories("Delete threshold record")
    public void testF() {
        String thresholdName = "Blood Glucose";

        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapDeleteButton();
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().verifyThatThresholdModuleRecordIsNoLongerDisplayed(thresholdName);
    }

    @Ignore
    @Test
    @Stories("Tap done")
    public void testG() {
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapDoneButton();
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().verifyThatThereAreNoActiveThresholds();
    }

    @Ignore
    @Test
    @Stories("Tap less to collapse threshold view")
    public void testH() {
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapLessButton();
    } */
}