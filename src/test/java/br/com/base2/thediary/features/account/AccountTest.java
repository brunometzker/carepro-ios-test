package br.com.base2.thediary.features.account;

import br.com.base2.thediary.components.modals.LoginModal;
import br.com.base2.thediary.fluentlenium.AllureReportRunner;
import br.com.base2.thediary.pages.AccountTab;
import br.com.base2.thediary.pages.DashboardTab;
import br.com.base2.thediary.pages.InitialScreen;
import br.com.base2.thediary.sharedsteps.SharedSteps;
import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.annotation.Page;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Created by leonardoamaral on 07/04/17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Features("Account")
public class AccountTest extends AllureReportRunner {
    @Page
    private InitialScreen initialScreen;
    @Page
    private SharedSteps sharedSteps;
    @Page
    private DashboardTab dashboardTab;
    @Page
    private AccountTab accountTab;

    @Test
    @Stories("Log out of the application")
    public void testA_loggingOutOfApplication() {
        String login = "autotest@thediary.com";
        String password = "Healthy15";

        sharedSteps.login(login, password);

        dashboardTab.atCareProTabBar().goToAccount();

        accountTab.tapOnAccountDetailsButton();
        accountTab.tapOnLogoutButton();

        initialScreen.verifyThatScreenIsDisplayed();
    }
}
