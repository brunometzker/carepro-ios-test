package br.com.base2.thediary.features.taskcalendar;

import org.fluentlenium.adapter.junit.FluentTest;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Created by leonardoamaral on 12/04/17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Features("Task Calendar Care Manager")
public class TaskCalendarCareManagerTest extends FluentTest {
    @Test
    @Stories("Add new task without reminder and different assignee")
    public void testA() {}

    @Test
    @Stories("Add new task with reminder and different assignee")
    public void testB() {}

    @Test
    @Stories("Add new task without reminder")
    public void testC() {}

    @Test
    @Stories("Add new task with reminder")
    public void testD() {}
}
