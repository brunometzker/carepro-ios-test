package br.com.base2.thediary.features.thresholds;

import br.com.base2.thediary.fluentlenium.AllureReportRunner;
import br.com.base2.thediary.pages.DashboardTab;
import br.com.base2.thediary.pages.OwnersTab;
import br.com.base2.thediary.sharedsteps.SharedSteps;
import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.annotation.Page;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Created by leonardoamaral on 22/03/17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Features("Body Temperature Thresholds")
public class BodyTemperatureThresholdTest extends AllureReportRunner {
    private String login = "autotest@thediary.com";
    private String password = "Healthy15";

    @Page private SharedSteps sharedSteps;
    @Page private DashboardTab dashboardTab;
    @Page private OwnersTab ownersTab;

    @Test
    @Stories("Tap new button to create body temperature threshold")
    public void testA_tapNewButtonToCreateBodyTemperatureThreshold() {
        String ownerName = "MarlonP2";

        sharedSteps.login(login, password);

        dashboardTab.atCareProTabBar().goToOwners();
        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.tapOwnerRecord(ownerName);
        ownersTab.tapOnCancelSearch();
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapNewButton();

        ownersTab.atNewThresholdModal().verifyThatModalIsDisplayed();
    }

    /*
    @Ignore
    @Test
    @Stories("Fill in threshold details for body temperature")
    public void testB() throws InterruptedException {
        String thresholdModule = "Body Temperature";
        String severityLevel = "Medium";
        String notificationMethod = "Email";
        String minimunLimit = "10";
        String maximumLimit = "20";

        ownersTab.atNewThresholdModal().selectThresholdModule(thresholdModule);
        ownersTab.atNewThresholdModal().selectSeverityLevel(severityLevel);
        ownersTab.atNewThresholdModal().selectNotificationMethod(notificationMethod);
        ownersTab.atNewThresholdModal().fillInMinimunLimit(minimunLimit);
        ownersTab.atNewThresholdModal().fillInMaximumLimit(maximumLimit);
        ownersTab.atNewThresholdModal().tapSaveButton();

        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().verifyThatThereAreActiveThresholds();
    }

    @Ignore
    @Test
    @Stories("Tap more to expand threshold view")
    public void testC() {
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().tapMoreButton();
        ownersTab.atOwnerDetails().atDashboardSection().atThresholdBlock().verifyThatActionButtonsAreDisplayed();
    }

    @Ignore
    @Test
    @Stories("Click on body temperature threshold to edit details")
    public void testE() {}

    @Ignore
    @Test
    @Stories("Tap edit, then red icon next to the threshold you want to delete")
    public void testF() {}

    @Ignore
    @Test
    @Stories("Tap done")
    public void testD() {}

    @Ignore
    @Test
    @Stories("Tap less to collapse threshold view")
    public void testG() {} */
}
