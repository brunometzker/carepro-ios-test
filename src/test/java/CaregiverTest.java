import br.com.base2.thediary.pages.DashboardTab;
import br.com.base2.thediary.pages.InitialScreen;
import br.com.base2.thediary.components.modals.LoginModal;
import br.com.base2.thediary.pages.OwnersTab;
import br.com.base2.thediary.sharedsteps.SharedSteps;
import br.com.base2.thediary.utils.Utilities;
import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.annotation.Page;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by bmetzker on 24/02/17.
 */
public class CaregiverTest extends FluentTest {
    private static Map<String, String> testRunConstants = new HashMap<String, String>();

    @Page InitialScreen initialScreen;
    @Page LoginModal loginModal;
    @Page DashboardTab dashboardTab;
    @Page OwnersTab ownersTab;
    @Page SharedSteps sharedSteps;

    @BeforeClass
    public static void setUp() {
        testRunConstants.put("login", "autotest@thediary.com");
        testRunConstants.put("password", "Healthy15");
    }

    @Test
    public void shouldDisplayOwnerWhenSearchingForHim() {
        String ownerName = "MarlonP2";

        sharedSteps.login(testRunConstants.get("login"), testRunConstants.get("password"));
        dashboardTab.atCareProTabBar().goToOwners();
        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.verifyThatOwnerRecordIsDisplayed(ownerName);
    }

    @Test
    public void shouldDisplayCreateCareplanOptionWhenOwnerHasNoCareplan() {
        String ownerName = "MarlonP3";

        sharedSteps.login(testRunConstants.get("login"), testRunConstants.get("password"));
        dashboardTab.atCareProTabBar().goToOwners();
        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.tapOwnerRecord(ownerName);
        ownersTab.tapOnCancelSearch();
        ownersTab.atOwnerDetails().tapOnCareplanSection();
        ownersTab.atOwnerDetails().verifyThatCreateCareplanButtonIsDisplayed();
    }

    @Test
    public void shouldDisplayExistingCareplanWhenOwnerHasOne() {
        String ownerName = "MarlonP1";

        sharedSteps.login(testRunConstants.get("login"), testRunConstants.get("password"));
        dashboardTab.atCareProTabBar().goToOwners();
        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.tapOwnerRecord(ownerName);
        ownersTab.tapOnCancelSearch();
        ownersTab.atOwnerDetails().tapOnCareplanSection();
        ownersTab.atOwnerDetails().verifyThatExistingCareplanIsDisplayed();
    }

    @Test
    public void shouldDisplayExistingGoal() {
        String ownerName = "MarlonP1";

        sharedSteps.login(testRunConstants.get("login"), testRunConstants.get("password"));
        dashboardTab.atCareProTabBar().goToOwners();
        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.tapOwnerRecord(ownerName);
        ownersTab.tapOnCancelSearch();
        ownersTab.atOwnerDetails().tapOnCareplanSection();
        ownersTab.atOwnerDetails().verifyThatExistingGoalIsDisplayed();
    }

    @Test
    public void shouldDisplayOwnerAsActive() {
        String ownerName = "Marlon P1";

        sharedSteps.login(testRunConstants.get("login"), testRunConstants.get("password"));
        dashboardTab.atCareProTabBar().goToOwners();
        ownersTab.fillSearchFieldWith(ownerName);
        ownersTab.tapOwnerRecord(ownerName);
        ownersTab.tapOnCancelSearch();
        ownersTab.atOwnerDetails().tapOnCareplanSection();
        ownersTab.atOwnerDetails().verifyThatOwnerIsActive();
    }

    @Test
    public void shouldDisplayNewOwnerWhenOneIsCreated() {
        String newOwnerName = Utilities.generateRandomAlphanumericString();
        String newOwnerSurname = Utilities.generateRandomAlphanumericString();
        String newOwnerGender = "Male";
        String newOwnerPatientGroup = "Default Patient Group";
        String newOwnerCareManager = "Autotest Marlon";

        sharedSteps.login(testRunConstants.get("login"), testRunConstants.get("password"));

        dashboardTab.atCareProTabBar().goToOwners();

        ownersTab.tapAddOwnerButton();

        ownersTab.atCreateOwnerModal().fillFirstNameField(newOwnerName);
        ownersTab.atCreateOwnerModal().fillSurnameField(newOwnerSurname);
        ownersTab.atCreateOwnerModal().selectGender(newOwnerGender);
        ownersTab.atCreateOwnerModal().selectPatientGroup(newOwnerPatientGroup);
        ownersTab.atCreateOwnerModal().tapCreateOwnerButton();
        ownersTab.atCreateOwnerModal().selectCareManager(newOwnerCareManager);
        ownersTab.atCreateOwnerModal().tapSaveButton();

        ownersTab.fillSearchFieldWith(newOwnerName);
        ownersTab.verifyThatOwnerRecordIsDisplayed(newOwnerName);
    }

    @Test
    public void shouldDisplayOwnerAsNewWhenItsANewlyCreatedOwner() {
        String newOwnerName = Utilities.generateRandomAlphanumericString();
        String newOwnerSurname = Utilities.generateRandomAlphanumericString();
        String newOwnerGender = "Male";
        String newOwnerPatientGroup = "Default Patient Group";
        String newOwnerCareManager = "Autotest Marlon";

        sharedSteps.login(testRunConstants.get("login"), testRunConstants.get("password"));

        dashboardTab.atCareProTabBar().goToOwners();

        ownersTab.tapAddOwnerButton();

        ownersTab.atCreateOwnerModal().fillFirstNameField(newOwnerName);
        ownersTab.atCreateOwnerModal().fillSurnameField(newOwnerSurname);
        ownersTab.atCreateOwnerModal().selectGender(newOwnerGender);
        ownersTab.atCreateOwnerModal().selectPatientGroup(newOwnerPatientGroup);
        ownersTab.atCreateOwnerModal().tapCreateOwnerButton();
        ownersTab.atCreateOwnerModal().selectCareManager(newOwnerCareManager);
        ownersTab.atCreateOwnerModal().tapSaveButton();

        ownersTab.fillSearchFieldWith(newOwnerName);
        ownersTab.tapOwnerRecord(newOwnerName);

        ownersTab.atOwnerDetails().verifyThatOwnerIsNew();
    }

    @Test
    public void shouldDisplayEnrollmentTaskOnNewOwnerCareplanTab() {
        //need to ask Roxanna about this test
    }

    @Test
    public void shouldDisplayNewCareplanWhenOneIsCreated() {
        String newOwnerName = Utilities.generateRandomAlphanumericString();
        String newOwnerSurname = Utilities.generateRandomAlphanumericString();
        String newOwnerGender = "Male";
        String newOwnerPatientGroup = "Default Patient Group";
        String newOwnerCareManager = "Autotest Marlon";
        String careplanFocus = "My CarePlan";

        sharedSteps.login(testRunConstants.get("login"), testRunConstants.get("password"));

        dashboardTab.atCareProTabBar().goToOwners();

        ownersTab.tapAddOwnerButton();

        ownersTab.atCreateOwnerModal().fillFirstNameField(newOwnerName);
        ownersTab.atCreateOwnerModal().fillSurnameField(newOwnerSurname);
        ownersTab.atCreateOwnerModal().selectGender(newOwnerGender);
        ownersTab.atCreateOwnerModal().selectPatientGroup(newOwnerPatientGroup);
        ownersTab.atCreateOwnerModal().tapCreateOwnerButton();
        ownersTab.atCreateOwnerModal().selectCareManager(newOwnerCareManager);
        ownersTab.atCreateOwnerModal().tapSaveButton();

        ownersTab.fillSearchFieldWith(newOwnerName);
        ownersTab.tapOwnerRecord(newOwnerName);

        ownersTab.atOwnerDetails().tapOnCareplanSection();
        ownersTab.atOwnerDetails().atCarePlanSection().tapCreateCarePlanButton();

        ownersTab.atCreateCarePlanModal().fillCarePlanFocus(careplanFocus);
        ownersTab.atCreateCarePlanModal().tapSaveButton();

        ownersTab.atOwnerDetails().atCarePlanSection().verifyThatCarePlanIsDisplayed(careplanFocus);
    }

    @Test
    public void shouldDisplayNewGoalWhenOneIsCreated() {
        String newOwnerName = Utilities.generateRandomAlphanumericString();
        String newOwnerSurname = Utilities.generateRandomAlphanumericString();
        String newOwnerGender = "Male";
        String newOwnerPatientGroup = "Default Patient Group";
        String newOwnerCareManager = "Autotest Marlon";
        String careplanFocus = "My CarePlan";
        String goal = "Quit Smoking";
        String problem = "Asthma";

        sharedSteps.login(testRunConstants.get("login"), testRunConstants.get("password"));

        dashboardTab.atCareProTabBar().goToOwners();

        ownersTab.tapAddOwnerButton();

        ownersTab.atCreateOwnerModal().fillFirstNameField(newOwnerName);
        ownersTab.atCreateOwnerModal().fillSurnameField(newOwnerSurname);
        ownersTab.atCreateOwnerModal().selectGender(newOwnerGender);
        ownersTab.atCreateOwnerModal().selectPatientGroup(newOwnerPatientGroup);
        ownersTab.atCreateOwnerModal().tapCreateOwnerButton();
        ownersTab.atCreateOwnerModal().selectCareManager(newOwnerCareManager);
        ownersTab.atCreateOwnerModal().tapSaveButton();

        ownersTab.fillSearchFieldWith(newOwnerName);
        ownersTab.tapOwnerRecord(newOwnerName);

        ownersTab.atOwnerDetails().tapOnCareplanSection();
        ownersTab.atOwnerDetails().atCarePlanSection().tapCreateCarePlanButton();

        ownersTab.atCreateCarePlanModal().fillCarePlanFocus(careplanFocus);
        ownersTab.atCreateCarePlanModal().tapSaveButton();

        ownersTab.atOwnerDetails().atCarePlanSection().verifyThatCarePlanIsDisplayed(careplanFocus);

        ownersTab.atOwnerDetails().atCarePlanSection().tapAddProblemButton();

        ownersTab.atCreateGoalModal().fillGoalField(goal);
        ownersTab.atCreateGoalModal().fillProblemField(problem);
        ownersTab.atCreateGoalModal().tapSaveButton();

        ownersTab.atOwnerDetails().verifyThatCreatedGoalIsDisplayed(goal);
    }

    @Test
    public void shouldRemoveGoalWhenItIsDeleted() {
        //Goal Creation
        String newOwnerName = Utilities.generateRandomAlphanumericString();
        String newOwnerSurname = Utilities.generateRandomAlphanumericString();
        String newOwnerGender = "Male";
        String newOwnerPatientGroup = "Default Patient Group";
        String newOwnerCareManager = "Autotest Marlon";
        String careplanFocus = "My CarePlan";
        String goal = "Quit Smoking";
        String problem = "Asthma";

        sharedSteps.login(testRunConstants.get("login"), testRunConstants.get("password"));

        dashboardTab.atCareProTabBar().goToOwners();

        ownersTab.tapAddOwnerButton();

        ownersTab.atCreateOwnerModal().fillFirstNameField(newOwnerName);
        ownersTab.atCreateOwnerModal().fillSurnameField(newOwnerSurname);
        ownersTab.atCreateOwnerModal().selectGender(newOwnerGender);
        ownersTab.atCreateOwnerModal().selectPatientGroup(newOwnerPatientGroup);
        ownersTab.atCreateOwnerModal().tapCreateOwnerButton();
        ownersTab.atCreateOwnerModal().selectCareManager(newOwnerCareManager);
        ownersTab.atCreateOwnerModal().tapSaveButton();

        ownersTab.fillSearchFieldWith(newOwnerName);
        ownersTab.tapOwnerRecord(newOwnerName);

        ownersTab.atOwnerDetails().tapOnCareplanSection();
        ownersTab.atOwnerDetails().atCarePlanSection().tapCreateCarePlanButton();

        ownersTab.atCreateCarePlanModal().fillCarePlanFocus(careplanFocus);
        ownersTab.atCreateCarePlanModal().tapSaveButton();

        ownersTab.atOwnerDetails().atCarePlanSection().verifyThatCarePlanIsDisplayed(careplanFocus);

        ownersTab.atOwnerDetails().atCarePlanSection().tapAddProblemButton();

        ownersTab.atCreateGoalModal().fillGoalField(goal);
        ownersTab.atCreateGoalModal().fillProblemField(problem);
        ownersTab.atCreateGoalModal().tapSaveButton();

        ownersTab.atOwnerDetails().verifyThatCreatedGoalIsDisplayed(goal);
        //Goal Creation Ending

        ownersTab.atOwnerDetails().atCarePlanSection().atGoalBlock().tapShowMoreButton(goal);
        ownersTab.atOwnerDetails().atCarePlanSection().atGoalBlock().tapSettingButton(goal);

        ownersTab.atGoalSettingModal().tapDeleteButton();

        ownersTab.atOwnerDetails().atCarePlanSection().verifyThatGoalIsNoLongerDisplayed(goal);
    }

    @Test
    public void shouldDisplayCareteamTaskWhenOneIsCreated() {
        //login and generation of a careplan with a goal

        String goal = "Quit Smoking";
        String taskType = "Appointment";
        String taskTitle = "Placeholder Title";
        String taskDescription = "Placeholder Description";

        ownersTab.atOwnerDetails().atCarePlanSection().atGoalBlock().tapShowMoreButton(goal);
        ownersTab.atOwnerDetails().atCarePlanSection().atGoalBlock().tapAddCareteamTaskButton();
        ownersTab.atTaskModal().selectTaskType(taskType);
        ownersTab.atTaskModal().fillTaskTitle(taskTitle);
        ownersTab.atTaskModal().fillTaskDescription(taskDescription);
        ownersTab.atTaskModal().tapSaveButton();

        ownersTab.atOwnerDetails().atCarePlanSection().atGoalBlock().verifyThatCareteamTaskIsDisplayed();
    }

    @Test
    public void shouldDisplayTaskAsActive() {
        //login and generation of a careplan with a goal
        //careteam task goal creation

        ownersTab.atOwnerDetails().atCarePlanSection().atGoalBlock().verifyThatCareteamTaskIsActive();
    }

    @Test
    public void shouldDisplayTaskAsCompleted() {
        //login and generation of a careplan with a goal
        //careteam task goal creation
        String careteamTask = "";
        String status = "Completed";

        ownersTab.atOwnerDetails().atCarePlanSection().atGoalBlock().tapCareteamTask(careteamTask);
        ownersTab.atCareteamTaskModal().tapStatusLabel();
        ownersTab.atCareteamTaskStatusModal().tapStatusField();
        ownersTab.atCareteamTaskStatusModal().tapStatusOption(status);
        ownersTab.atCareteamTaskModal().tapDoneButton();
        ownersTab.atOwnerDetails().atCarePlanSection().atGoalBlock().verifyThatCareteamTaskIsCompleted();
    }

    @Test
    public void shouldRemoveTaskWhenItIsDeleted() {
        //login and generation of a careplan with a goal
        //careteam task goal creation
        String careteamTask = "";

        ownersTab.atOwnerDetails().atCarePlanSection().atGoalBlock().tapCareteamTask(careteamTask);
        ownersTab.atCareteamTaskModal().tapDeleteButton();
        ownersTab.atOwnerDetails().atCarePlanSection().atGoalBlock().verifyThatCareteamTaskIsNoLongerDisplayed();
    }

    @Test
    public void shouldRemoveServiceTypeWhenItIsDeleted() {}

    @Test
    public void shouldDisplayEnrollmentTaskAsCompleteWhenStatusIsUpdated() {
        //get owner with enrollment task
        String enrollmentTask = "";
        String newStatus = "Completed";

        ownersTab.atOwnerDetails().tapOnCareplanSection();
        ownersTab.atOwnerDetails().atCarePlanSection().atEnrollmentTaskBlock().tapEnrollmentTask(enrollmentTask);
        ownersTab.atEnrollmentTaskModal().tapStatusLabel();
        ownersTab.atEnrollmentTaskStatusModal().updateStatus(newStatus);
        ownersTab.atEnrollmentTaskModal().verifyThatStatusIs(newStatus);
    }
}