import br.com.base2.thediary.features.login.LoginTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by leonardoamaral on 10/03/17.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    LoginTest.class,
    CaregiverTest.class
})
public class TestRun {}
